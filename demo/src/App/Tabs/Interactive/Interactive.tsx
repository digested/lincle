import React, {
  type CSSProperties,
  type FunctionComponent,
  memo,
  type ReactElement
} from 'react';
import Button from 'src/Components/Generics/Button';
import Container from 'src/Components/Generics/Container';
import Text from 'src/Components/Generics/Text';
import {
  Edge,
  Edges,
  Graph,
  Node,
  Nodes
} from 'src/Components/LincleInteractive';
import {
  MiniMap
} from 'src/Components/LincleMinimap';

const styles: {
  [key: string]: CSSProperties;
} = {
  node: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: 'rgba(0, 0, 0, 1)',
    borderStyle: 'solid',
    borderWidth: 1,
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    // boxShadow: '0 3px 6px rgba(0 0 0 / 16%), 0 3px 6px rgba(0 0 0 / 23%)',
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    shadowColor: 'rgba(0 0 0 / 16%)',
    shadowOpacity: 16,
    shadowRadius: 50,
    width: '100%'
  }
};

styles.nodeOval = {
  ...styles.node,
  borderRadius: 50
};

const Interactive: FunctionComponent = (): ReactElement => {
  return (
    <Graph
      edgeFrequency={16}
      id='YetAnotherDiagram'
      nodeFrequency={16}
      nodeHeight={50}
      nodeWidth={50}
      shape='oval'
      snap={false}
    >
      <Edges>
        <Edge
          id={1}
          key={1}
          sourceId={1}
          targetId={2}
        >
          <Button>
            Bridge
          </Button>
        </Edge>
        <Edge
          id={2}
          key={2}
          line='direct'
          sourceId={2}
          targetId={3}
        />
      </Edges>
      <Nodes>
        <Node
          id={1}
          key={1}
          shape='rectangle'
          x={50}
          y={50}
        >
          <Container
            style={styles.node as CSSProperties}
          >
            <Text>
              Node 1
            </Text>
          </Container>
        </Node>
        <Node
          height={100}
          id={2}
          key={2}
          line='direct'
          width={100}
          x={100}
          y={150}
        >
          <Container
            style={styles.nodeOval as CSSProperties}
          >
            <Text>
              Node 2
            </Text>
          </Container>
        </Node>
        <Node
          id={3}
          key={3}
          line='curve'
          x={150}
          y={350}
        >
          <Container
            style={styles.nodeOval as CSSProperties}
          >
            <Text>
              Node 3
            </Text>
          </Container>
        </Node>
      </Nodes>
      <MiniMap />
    </Graph>
  );
};

Interactive.displayName = 'LincleDemoInteractive';

export default memo(Interactive);
