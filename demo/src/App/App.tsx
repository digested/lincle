import Paragraph from '../Components/Generics/Paragraph';
import Preliminary from '../Components/Preliminary';
import {
  ReactRouterDigestAppBar as AppBar,
  ReactRouterDigestDrawer as Drawer,
  ReactRouterDigestDrawerBar as DrawerBar,
  ReactRouterDigest,
  ReactRouterDigestTab as Tab
} from '../Components/ReactRouter';
import Interactive from './Tabs/Interactive';
import Static from './Tabs/Static';
import React, {
  type FunctionComponent
} from 'react';

const App: FunctionComponent = () => {
  return (
    <Preliminary>
      <ReactRouterDigest
        swipe={false}
        tabIndex={0}
      >
        <AppBar />
        <DrawerBar />
        <Drawer
          drawerId='dials'
          tabId='tab1'
        >
          <Paragraph>
            tab1
          </Paragraph>
        </Drawer>
        <Drawer
          drawerId='dials'
          tabId='tab2'
        >
          <Paragraph>
            tab2
          </Paragraph>
        </Drawer>
        <Tab
          swipe={false}
          tabId='Static'
          tabPath='tabs/:tabId'
          title='Static'
        >
          <Static />
        </Tab>
        <Tab
          swipe={false}
          tabId='Interactive'
          tabPath='tabs/:tabId'
          title='Interactive'
        >
          <Interactive />
        </Tab>
        <Drawer
          drawerId='drawer2'
        >
          <Paragraph>
            drawer2
          </Paragraph>
        </Drawer>
      </ReactRouterDigest>
    </Preliminary>
  );
};

export default App;
