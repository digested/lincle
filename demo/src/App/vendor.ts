/*
  eslint-disable
  import/no-unassigned-import
*/

import '@lincle/react-web-base/dist/styles.g.css';
import '@lincle/react-web-interactive/dist/styles.g.css';
import '@lincle/react-web-minimap/dist/styles.g.css';
import 'react-modern-drawer/dist/index.css';
import 'swiper/css';

export {};
