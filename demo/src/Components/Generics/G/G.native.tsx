import React, {
  type FunctionComponent,
  type SVGProps as ReactSvgProps
} from 'react';
import {
  StyleSheet
} from 'react-native';
import {
  G as NativeG,
  type SvgProps as NativeSvgProps
} from 'react-native-svg';

const styles = StyleSheet.create({
  gStyle: {
    height: 100,
    overflow: 'visible',
    pointerEvents: 'none',
    width: 100
  }
});

// eslint-disable-next-line id-length
const G: FunctionComponent<SvgProps> = (props) => {
  return (
    <NativeG
      {...props as NativeSvgProps}
      style={styles.gStyle}
    />
  );
};

export {
  G
};

export type SvgProps = NativeSvgProps & Partial<WebGProps>;

type WebGProps = ReactSvgProps<SVGGElement>;
