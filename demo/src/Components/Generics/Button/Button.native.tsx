import React, {
  type FunctionComponent,
  type HTMLAttributes,
  useCallback
} from 'react';
import {
  Button as NativeButton,
  type ButtonProps as NativeButtonProps
} from 'react-native';

const Button: FunctionComponent<ButtonProps> = ({
  children,
  onClick,
  ...props
}) => {
  const handleOnPress = useCallback(
    () => {
      if (onClick) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        onClick();
      }
    },
    [
      onClick
    ]
  );

  return (
    <NativeButton
      onPress={handleOnPress}
      {...props as NativeButtonProps}
      title={children}
    />
  );
};

export {
  Button
};

export type ButtonProps = NativeButtonProps & Partial<Omit<WebTextProps, 'children'>> & { readonly children: string };

type WebTextProps = HTMLAttributes<HTMLButtonElement>;
