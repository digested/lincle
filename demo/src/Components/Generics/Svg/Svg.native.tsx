import React, {
  type FunctionComponent,
  type SVGProps as ReactSvgProps
} from 'react';
import {
  StyleSheet
} from 'react-native';
import {
  Svg as NativeSvg,
  type SvgProps as NativeSvgProps
} from 'react-native-svg';

const styles = StyleSheet.create({
  svgStyle: {
    height: 100,
    overflow: 'visible',
    pointerEvents: 'none',
    width: 100
  }
});

const Svg: FunctionComponent<SvgProps> = (props) => {
  return (
    <NativeSvg
      {...props as NativeSvgProps}
      style={styles.svgStyle}
    />
  );
};

export {
  Svg
};

export type SvgProps = NativeSvgProps & Partial<WebSvgProps>;

type WebSvgProps = ReactSvgProps<SVGSVGElement>;
