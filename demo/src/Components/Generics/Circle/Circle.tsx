import React, {
  type FunctionComponent,
  type SVGProps
} from 'react';

const Circle: FunctionComponent<CircleProps> = (props) => {
  return (
    <circle
      {...props}
    />
  );
};

export {
  Circle
};

export type CircleProps = SVGProps<SVGCircleElement>;
