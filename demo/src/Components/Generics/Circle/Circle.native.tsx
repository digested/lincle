import React, {
  type FunctionComponent,
  type SVGProps,
  useCallback
} from 'react';
import {
  type CircleProps as NativeCircleProps,
  Circle as SvgCircle
} from 'react-native-svg';

const Circle: FunctionComponent<CircleProps> = ({
  onClick,
  ...props
}) => {
  const handleOnPress = useCallback(
    () => {
      if (onClick) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        onClick();
      }
    },
    [
      onClick
    ]
  );

  return (
    <SvgCircle
      {...props as NativeCircleProps}
      onPress={handleOnPress}
    />
  );
};

export {
  Circle
};

export type CircleProps = NativeCircleProps & Partial<WebCircleProps>;

type WebCircleProps = SVGProps<SVGCircleElement>;
