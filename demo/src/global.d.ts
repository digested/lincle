declare module '*.css';
declare module '*.scss';

// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
// @ts-ignore
declare let System: {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  import: any;
};

declare module '*.json' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const value: any;
  // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
  // @ts-ignore
  export default value;
}

// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
// @ts-ignore
declare let BUILD: {
  DATE: string;
};

declare let PACKAGE: {
  DESCRIPTION: string;
  NAME: string;
  TITLE: string;
  VERSION: string;
};
