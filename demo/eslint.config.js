/*
  eslint-disable
  unicorn/prefer-module
*/

// eslint-disable-next-line n/no-extraneous-require,import/no-extraneous-dependencies
const config = require('@digest/eslint-config');

module.exports = [
  {
    ignores: [
      'android/*',
      'ios/*'
    ]
  },
  ...config
];
