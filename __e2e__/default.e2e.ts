/*
	eslint-disable
	canonical/filename-match-exported,
	unicorn/prevent-abbreviations
*/

import type {
	NightwatchBrowser,
	NightwatchTests
} from 'nightwatch';

const test: NightwatchTests = {
	'Render test' (browser: NightwatchBrowser): void {
		browser
			.url(browser.launch_url)
			.waitForElementVisible(
				'body',
				1_000
			)
			.resizeWindow(
				1_024,
				768
			)
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			.assert.screenshotIdenticalToBaseline(
				'#app',
				undefined,
				{
					threshold: 0.3
				}
			)
			.end();
	}
};

export default test;
