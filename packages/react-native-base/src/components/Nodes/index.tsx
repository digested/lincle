import {
  type NodesProps
} from '../../shared';
import {
  type FunctionComponent
} from 'react';

const Nodes: FunctionComponent<NodesProps> = ({
  children
}) => {
  return (
    <>
      {children}
    </>
  );
};

Nodes.displayName = 'LincleNodes';

export {
  Nodes
};
