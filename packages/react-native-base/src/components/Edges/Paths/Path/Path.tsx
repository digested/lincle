import {
  generateOrigins,
  type PathProps,
  useDiagramId,
  useUpdateEdge
} from '../../../../shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';
import {
  G,
  Path as NativePath
} from 'react-native-svg';

const Path: FunctionComponent<PathProps> = ({
  center = true,
  edgeId,
  line,
  markerEnd: givenMarkerEnd,
  markerStart: givenMarkerStart,
  source,
  target
}) => {
  const diagramId = useDiagramId();
  const updateEdge = useUpdateEdge();

  const origins = useMemo(
    () => {
      if (
        source &&
        target &&
        line
      ) {
        const generatedOrigins = generateOrigins(
          source,
          target,
          line,
          center
        );

        return generatedOrigins;
      }

      return undefined;
    },
    [
      center,
      line,
      source,
      target
    ]
  );

  useEffect(
    () => {
      if (
        origins?.center &&
        updateEdge
      ) {
        updateEdge(
          edgeId,
          {
            center: origins.center
          }
        );
      }
    },
    [
      edgeId,
      origins?.center,
      updateEdge
    ]
  );

  const markerEnd = useMemo(
    () => {
      const id = diagramId ?
        `${diagramId}-` :
        '';

      return givenMarkerEnd ??
        `url(#diagram-${id}marker-arrow)`;
    },
    [
      diagramId,
      givenMarkerEnd
    ]
  );

  const markerStart = useMemo(
    () => {
      const id = diagramId ?
        `${diagramId}-` :
        '';

      return givenMarkerStart ??
        `url(#diagram-${id}marker-circle)`;
    },
    [
      diagramId,
      givenMarkerStart
    ]
  );

  return (
    <G>
      <NativePath
        d={origins?.path}
        fill='transparent'
        id={diagramId + '1'}
        opacity={0}
        pointerEvents='box-none'
        stroke='black'
        strokeWidth={30}
      />
      <NativePath
        d={origins?.path}
        fill='transparent'
        id={diagramId + '2'}
        markerEnd={markerEnd}
        markerStart={markerStart}
        pointerEvents='box-none'
        stroke='black'
      />
    </G>
  );
};

Path.displayName = 'LinclePath';

export default Path;
