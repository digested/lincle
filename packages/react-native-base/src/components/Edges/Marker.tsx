import {
  useDiagramId
} from '../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  Circle,
  Marker as NativeMarker,
  Path
} from 'react-native-svg';

const Marker: FunctionComponent<object> = () => {
  const id = useDiagramId();

  const markerId = useMemo(
    () => {
      return id ?
        `${id}-` :
        '';
    },
    [
      id
    ]
  );

  const arrowId = useMemo(
    () => {
      return `diagram-${markerId}marker-arrow`;
    },
    [
      markerId
    ]
  );

  const circleId = useMemo(
    () => {
      return `diagram-${markerId}marker-circle`;
    },
    [
      markerId
    ]
  );

  return (
    <>
      <NativeMarker
        id={arrowId}
        markerHeight='10'
        markerUnits='strokeWidth'
        markerWidth='10'
        orient='auto'
        refX='9'
        refY='5'
      >
        <Path
          d='M 0 0 L 10 5 L 0 10 z'
          fill='black'
        />
      </NativeMarker>
      <NativeMarker
        id={circleId}
        markerHeight='10'
        markerUnits='strokeWidth'
        markerWidth='10'
        orient='auto'
        refX='5'
        refY='5'
      >
        <Circle
          cx='5'
          cy='5'
          fill='black'
          r='3'
        />
      </NativeMarker>
    </>
  );
};

Marker.displayName = 'LincleMarker';

export default Marker;
