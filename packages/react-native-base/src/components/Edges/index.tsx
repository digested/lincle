import {
  type EdgesProps
} from '../../shared';
import Marker from './Marker';
import Paths from './Paths';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import Svg, {
  Defs,
  G
} from 'react-native-svg';

const styles = StyleSheet.create({
  bridges: {
    bottom: 0,
    left: 0,
    overflow: 'visible',
    pointerEvents: 'box-none',
    position: 'absolute',
    right: 0,
    top: 0
  },
  edges: {
    backfaceVisibility: 'hidden',
    // filter: 'drop-shadow(0 3px 6px rgba(0 0 0 / 16%)) drop-shadow(0 3px 6px rgba(0 0 0 23%))',
    overflow: 'visible',

    pointerEvents: 'box-none'
  }
});

const Edges: FunctionComponent<EdgesProps> = ({
  children,
  scale = 1,
  translate = {
    x: 0,
    y: 0
  }
}) => {
  const {
    style,
    transform
  } = useMemo(
    () => {
      const tx = translate.x;
      const ty = translate.y;
      const tz = scale;

      return {
        style: {
          ...styles.bridges,
          pointerEvents: 'box-none' as const,
          transform: `translateX(${tx}px) translateY(${ty}px) scale(${tz})`,
          transformOrigin: 'top left'
        },
        transform: `translate(${tx}, ${ty}) scale(${tz})`
      };
    },
    [
      translate.x,
      translate.y,
      scale
    ]
  );

  return (
    <>
      <Svg
        style={styles.edges}
      >
        <G
          transform={transform}
        >
          <Defs>
            <Marker />
          </Defs>
          <Paths />
        </G>
      </Svg>
      <View
        style={style}
      >
        {children}
      </View>
    </>
  );
};

Edges.displayName = 'LincleEdges';

export {
  Edges
};

export {
  default as Marker
} from './Marker';
export {
  Path
} from './Paths';
