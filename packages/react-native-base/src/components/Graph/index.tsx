import {
  type GraphProps,
  Providers
} from '../../shared';
import Grid from './Grid';
import {
  type FunctionComponent
} from 'react';

const Graph: FunctionComponent<GraphProps> = ({
  children,
  edgeFrequency,
  grid,
  id,
  line,
  nodeFrequency,
  nodeHeight,
  nodeWidth,
  shape,
  showGrid
}) => {
  if (!id) {
    // eslint-disable-next-line no-console
    console.error('No ID provided to @lincle/base Graph!');

    return null;
  }

  const gird = showGrid === false ?
    null :
    <Grid />;

  return (
    <Providers
      edgeFrequency={edgeFrequency}
      grid={grid}
      id={id}
      line={line}
      nodeFrequency={nodeFrequency}
      nodeHeight={nodeHeight}
      nodeWidth={nodeWidth}
      shape={shape}
    >
      {gird}
      {children}
    </Providers>
  );
};

Graph.displayName = 'LincleGraph';

export {
  Graph
};

export {
  default as Grid
} from './Grid';
