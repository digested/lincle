import {
  type ProvidersProps
} from './Providers';
import {
  type PropsWithChildren,
  type ReactElement,
  type ReactNode
} from 'react';

export type Dimensions = {
  height: number;
  id: number | string;
  shape: Shape;
  width: number;
  x: number;
  y: number;
};

export type EdgeNodeProps = {
  height: number;
  id: number | string;
  shape: Shape;
  width: number;
  x: number;
  y: number;
};

export type EdgeProps = PropsWithChildren<{
  dash?: boolean;
  id: number | string;
  line?: Line;
  markerEnd?: string;
  markerStart?: string;
  path?: (
    source?: EdgeNodeProps,
    target?: EdgeNodeProps,
    children?: ReactNode,
  ) => ReactElement;
  sourceId: number | string;
  targetId: number | string;
}>;

export type EdgesProps = PropsWithChildren<{
  readonly dash?: boolean;
  readonly scale?: number;
  readonly translate?: {
    x: number;
    y: number;
  };
}>;

export type GraphProps = ProvidersProps & {
  showGrid?: boolean;
};

export type GridContextProps = GridType;

export type GridProps = PropsWithChildren<{
  scale?: number;
  xOffset?: number;
  yOffset?: number;
}>;

export type GridType = [
  number,
  number
];

export type Line = 'curve' | 'direct' | 'step';

export type NodeProps = PropsWithChildren<{
  height?: number;
  id: number | string;
  ref?: ((instance: HTMLDivElement | null) => void) | null | React.RefObject<HTMLDivElement | null>;
  shape?: Shape;
  track?: boolean;
  width?: number;
  x?: number;
  y?: number;
}>;

export type NodesDimensions = {
  [key: string]: Dimensions;
};

export type NodesProps = PropsWithChildren<object>;

export type PathProps = {
  center?: boolean;
  edgeId: number | string;
  line?: Line;
  markerEnd?: string;
  markerStart?: string;
  source?: EdgeNodeProps;
  target?: EdgeNodeProps;
};

export type Shape = 'oval' | 'rectangle';

export type Size = {
  bottom: number;
  height: number;
  left: number;
  right: number;
  top: number;
  width: number;
};
