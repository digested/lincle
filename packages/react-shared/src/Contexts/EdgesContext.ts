import {
  type EdgeProps
} from '../types';
import {
  createContext
} from 'react';

const EdgesContext = createContext<EdgesContextProps>({
  edges: {}
});

EdgesContext.displayName = 'LincleEdgesContext';

export {
  EdgesContext
};

export type Edge = Partial<Omit<EdgeProps, 'id'>> & {
  center?: {
    x: number;
    y: number;
  };
  className?: string;
  edgeId: number | string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  style?: any;
};

export type Edges = {
  [key: number | string]: Edge;
};

export type EdgesContextProps = {
  edges: Edges;
  removeEdge?: RemoveEdge;
  updateEdge?: UpdateEdge;
};

export type RemoveEdge = (
  edgeId: number | string
) => void;

export type UpdateEdge = (
  edgeId: number | string,
  edge: Partial<Edge>
) => void;

