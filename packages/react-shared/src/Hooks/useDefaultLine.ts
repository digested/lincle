import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDefaultLine = () => {
  const line = useContext(GraphContext).line;

  return line;
};

export {
  useDefaultLine
};
