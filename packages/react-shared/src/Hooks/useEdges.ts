import {
  EdgesContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useEdges = () => {
  const {
    edges
  } = useContext(EdgesContext);

  return edges;
};

export {
  useEdges
};
