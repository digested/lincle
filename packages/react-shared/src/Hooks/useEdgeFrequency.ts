import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useEdgeFrequency = () => {
  const {
    edgeFrequency
  } = useContext(GraphContext);

  return edgeFrequency;
};

export {
  useEdgeFrequency
};
