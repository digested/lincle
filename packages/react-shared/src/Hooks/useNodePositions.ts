import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useNodePositions = () => {
  const {
    nodePositions
  } = useContext(GraphContext);

  return nodePositions;
};

export {
  useNodePositions
};
