import {
  EdgesContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useUpdateEdge = () => {
  const {
    updateEdge
  } = useContext(EdgesContext);

  return updateEdge;
};

export {
  useUpdateEdge
};
