import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDiagramId = () => {
  const {
    id
  } = useContext(GraphContext);

  return id;
};

export {
  useDiagramId
};
