import {
  EdgesContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useEdge = (
  edgeId: number | string
) => {
  const {
    edges
  } = useContext(EdgesContext);

  return edges[edgeId];
};

export {
  useEdge
};
