export type Coordinates = {
  center?: {
    x: number;
    y: number;
  };
  path: string;
  source: {
    x: number;
    y: number;
  };
  target: {
    x: number;
    y: number;
  };
};

export type Line = {
  center?: {
    x: number;
    y: number;
  };
  path: string;
};

export type Orientation = 'horizontal' | 'vertical';

export type Points = {
  orientation: Orientation;
  source: {
    x: number;
    y: number;
  };
  target: {
    x: number;
    y: number;
  };
};
