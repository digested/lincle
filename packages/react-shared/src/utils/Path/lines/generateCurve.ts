/*
  eslint-disable
  id-length
*/

import generatePoints from './generatePoints';
import {
  type Coordinates,
  type Line
} from './types';
import {
  Bezier
} from 'bezier-js';

export const curveHorizontal = (
  rx1: number,
  ry1: number,
  rx2: number,
  ry2: number,
  center?: boolean
): Line => {
  const x1 = Math.round(rx1);
  const y1 = Math.round(ry1);
  const x2 = Math.round(rx2);
  const y2 = Math.round(ry2);

  const d = [];
  const mx = Math.round(x1 + (x2 - x1) / 2);

  const c = center ?
    new Bezier(
      x1,
      y1,
      mx,
      y1,
      mx,
      y2,
      x2,
      y2
    ).compute(0.5) :
    undefined;

  d.push(
    'M',
    x1,
    y1
  );
  d.push(
    'C',
    mx,
    y1,
    mx,
    y2,
    x2,
    y2
  );

  return {
    center: c,
    path: d.join(' ')
  };
};

export const curveVertical = (
  rx1: number,
  ry1: number,
  rx2: number,
  ry2: number,
  center?: boolean
): Line => {
  const x1 = Math.round(rx1);
  const y1 = Math.round(ry1);
  const x2 = Math.round(rx2);
  const y2 = Math.round(ry2);

  const d = [];
  const my = Math.round(y1 + (y2 - y1) / 2);

  const c = center ?
    new Bezier(
      x1,
      y1,
      x1,
      my,
      x2,
      my,
      x2,
      y2
    ).compute(0.5) :
    undefined;

  d.push(
    'M',
    x1,
    y1
  );
  d.push(
    'C',
    x1,
    my,
    x2,
    my,
    x2,
    y2
  );

  return {
    center: c,
    path: d.join(' ')
  };
};

export default (
  source: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  },
  target: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  },
  center?: boolean
): Coordinates => {
  const {
    orientation,
    source: sourcePoints,
    target: targetPoints
  } = generatePoints(
    source,
    target
  );

  return {

    ...orientation === 'horizontal' ?
      curveHorizontal(
        sourcePoints.x,
        sourcePoints.y,
        targetPoints.x,
        targetPoints.y,
        center
      ) :
      curveVertical(
        sourcePoints.x,
        sourcePoints.y,
        targetPoints.x,
        targetPoints.y,
        center
      ),
    source: sourcePoints,
    target: targetPoints
  };
};
