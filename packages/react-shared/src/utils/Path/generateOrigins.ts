import {
  type EdgeNodeProps,
  type Line
} from '../../types';
import {
  type Coordinates,
  generateCurve,
  generateDirect,
  generateStep
} from './lines';

const generateOrigins = (
  target: EdgeNodeProps,
  source: EdgeNodeProps,
  line: Line,
  center?: boolean
): Coordinates => {
  switch (
    line
  ) {
    case 'curve': {
      return generateCurve(
        source,
        target,
        center
      );
    }

    case 'direct': {
      return generateDirect(
        source,
        target,
        center
      );
    }

    case 'step':
    default: {
      return generateStep(
        source,
        target,
        center
      );
    }
  }
};

export default generateOrigins;
