import MiniMapSvg from '../MiniMapSvg';
import {
  type MiniMapPlainProps
} from '../types';
import {
  BlurView
} from '@react-native-community/blur';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const displayName = 'LincleMiniMap';

const {
  blurStyle,
  viewStyle: defaultViewStyle
} = StyleSheet.create({
  blurStyle: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0
  },
  viewStyle: {
    alignItems: 'center',
    bottom: 16,
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'relative',
    right: 16
  }
});

const MiniMap: FunctionComponent<MiniMapPlainProps> = ({
  gutter,
  height: givenMapWidth,
  lincleHeight = 0,
  lincleWidth = 0,
  node,
  style,
  width: givenMapHeight
}) => {
  const {
    height,
    width
  } = useMemo(
    () => {
      const piWidth = lincleWidth / Math.PI;
      const piHeight = lincleHeight / Math.PI;

      const calcWidth = givenMapWidth ?? Math.min(
        lincleWidth - 32,
        Math.max(
          Math.min(
            piWidth,
            piHeight
          ),
          128
        )
      );
      const calcHeight = givenMapHeight ?? Math.min(
        lincleHeight - 32,
        Math.max(
          Math.min(
            piWidth,
            piHeight / 1.5
          ),
          128
        )
      );

      return {
        height: calcWidth,
        width: calcHeight
      };
    },
    [
      givenMapHeight,
      givenMapWidth,
      lincleHeight,
      lincleWidth
    ]
  );

  const viewStyle = useMemo(
    () => {
      return {
        height,
        width,
        ...defaultViewStyle,
        ...style
      };
    },
    [
      height,
      style,
      width
    ]
  );

  return lincleWidth === 0 ||
    lincleHeight === 0 ?
    null :

    <View
      style={viewStyle}
    >
      <BlurView
        blurAmount={5}
        blurType='light'
        overlayColor=''
        style={blurStyle}
      />
      <MiniMapSvg
        gutter={gutter}
        height={height}
        lincleHeight={lincleHeight}
        lincleWidth={lincleWidth}
        node={node}
        width={width}
      />
    </View>;
};

MiniMap.displayName = displayName;

export default memo(MiniMap);
