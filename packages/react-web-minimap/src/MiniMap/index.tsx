import MiniMapPlain from '../MiniMapPlain';
import {
  type MiniMapProps
} from '../types';
import {
  type FunctionComponent,
  memo,
  type ReactElement,
  useMemo
} from 'react';
import {
  useResizeDetector
} from 'react-resize-detector';

const displayName = 'LincleMiniMap';

const MiniMap: FunctionComponent<MiniMapProps> = ({
  className: givenClassname,
  gutter,
  height,
  node,
  style,
  width
}): ReactElement => {
  const {
    height: lincleHeight = 0,
    ref,
    width: lincleWidth = 0
  } = useResizeDetector({
    refreshMode: 'debounce',
    refreshRate: 16
  });

  const className = useMemo(
    () => {
      return `lincle-minimap-container ${givenClassname}`;
    },
    [
      givenClassname
    ]
  );

  return (
    <div
      className='lincle-minimap'
      ref={ref}
    >
      <MiniMapPlain
        className={className}
        gutter={gutter}
        height={height}
        lincleHeight={lincleHeight}
        lincleWidth={lincleWidth}
        node={node}
        style={style}
        width={width}
      />
    </div>
  );
};

MiniMap.displayName = displayName;

export default memo(MiniMap);
