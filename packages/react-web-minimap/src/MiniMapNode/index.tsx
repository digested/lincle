import {
  type MiniMapNodeProps
} from '../types';
import Ellipse from './Ellipse';
import Rectangle from './Rectangle';
import {
  type FunctionComponent,
  memo
} from 'react';

const MiniMapNode: FunctionComponent<MiniMapNodeProps> = ({
  height,
  shape,
  width,
  x,
  y
}) => {
  return (
    shape === 'oval' ?

      <Ellipse
        height={height}
        width={width}
        x={x}
        y={y}
      /> :

      <Rectangle
        height={height}
        width={width}
        x={x}
        y={y}
      />

  );
};

MiniMapNode.displayName = 'LincleMiniMapNode';

export default memo(MiniMapNode);
