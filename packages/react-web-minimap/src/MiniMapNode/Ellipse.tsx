import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const MiniMapNodeEllipse: FunctionComponent<MiniMapNodeEllipseProps> = ({
  height,
  width,
  x,
  y
}) => {
  const cx = useMemo(
    () => {
      return x + width / 2;
    },
    [
      width,
      x
    ]
  );
  const cy = useMemo(
    () => {
      return y + height / 2;
    },
    [
      height,
      y
    ]
  );
  const rx = useMemo(
    () => {
      return width / 2;
    },
    [
      width
    ]
  );
  const ry = useMemo(
    () => {
      return height / 2;
    },
    [
      height
    ]
  );

  return (
    <ellipse
      cx={cx}
      cy={cy}
      fill='rgba(0, 0, 0, 0.6)'
      rx={rx}
      ry={ry}
    />
  );
};

MiniMapNodeEllipse.displayName = 'LincleMiniMapNodeEllipse';

type MiniMapNodeEllipseProps = {
  readonly height: number;
  readonly width: number;
  readonly x: number;
  readonly y: number;
};

export default memo(MiniMapNodeEllipse);
