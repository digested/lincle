import {
  type FunctionComponent,
  memo
} from 'react';

const MiniMapNodeRectangle: FunctionComponent<MiniMapNodeRectangleProps> = ({
  height,
  width,
  x,
  y
}) => {
  return (
    <rect
      fill='rgba(0, 0, 0, 0.6)'
      height={height}
      width={width}
      x={x}
      y={y}
    />
  );
};

MiniMapNodeRectangle.displayName = 'LincleMiniMapNodeRectangle';

type MiniMapNodeRectangleProps = {
  readonly height: number;
  readonly width: number;
  readonly x: number;
  readonly y: number;
};

export default memo(MiniMapNodeRectangle);
