import {
  type EdgeProps,
  useEdge,
  useRemoveEdge,
  useUpdateEdge
} from '../../shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const Edge: FunctionComponent<EdgeProps> = ({
  children,
  className,
  dash,
  id: edgeId,
  line,
  markerEnd,
  markerStart,
  // path,
  sourceId,
  targetId
}) => {
  const updateEdge = useUpdateEdge();
  const removeEdge = useRemoveEdge();
  const edge = useEdge(edgeId);

  useEffect(
    () => {
      return (): void => {
        if (removeEdge) {
          removeEdge(
            edgeId
          );
        }
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useEffect(
    () => {
      if (updateEdge) {
        updateEdge(
          edgeId,
          {
            className,
            dash,
            line,
            markerEnd,
            markerStart,
            sourceId,
            targetId
          }
        );
      }

      return (): void => {
        if (removeEdge) {
          removeEdge(
            edgeId
          );
        }
      };
    },
    [
      className,
      dash,
      edgeId,
      line,
      markerEnd,
      markerStart,
      removeEdge,
      sourceId,
      targetId,
      updateEdge
    ]
  );

  const style = useMemo<CSSProperties>(
    () => {
      return {
        left: edge?.center?.x,
        position: 'absolute',
        top: edge?.center?.y
      };
    },
    [
      edge?.center?.x,
      edge?.center?.y
    ]
  );

  return (
    <div
      style={style}
    >
      {children}
    </div>
  );
};

Edge.displayName = 'LincleEdge';

export {
  Edge
};
