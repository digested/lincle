import {
  type NodesProps
} from '../../shared';
import {
  type FunctionComponent
} from 'react';

const Nodes: FunctionComponent<NodesProps> = (
  {
    children
  }
// eslint-disable-next-line @typescript-eslint/promise-function-async
) => {
  return children;
};

Nodes.displayName = 'LincleNodes';

export {
  Nodes
};
