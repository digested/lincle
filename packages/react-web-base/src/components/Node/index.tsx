import {
  type NodeProps,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useEdgeSubscriber,
  useNodePositions
} from '../../shared';
import {
  forwardRef,
  type ForwardRefRenderFunction,
  useEffect,
  useMemo
} from 'react';

const Node: ForwardRefRenderFunction<HTMLDivElement, NodeProps> = (
  {
    children,
    className: givenClassName = '',
    height: givenHeight,
    id: nodeId,
    shape: givenShape,
    style: {
      node: nodeStyle
    } = {},
    width: givenWidth,
    x: givenX = 0,
    y: givenY = 0,
    ...additionalProperties
  }: NodeProps,
  ref
) => {
  const defaultHeight = useDefaultNodeHeight();
  const defaultShape = useDefaultShape();
  const defaultWidth = useDefaultNodeWidth();
  const edgeSubscriber = useEdgeSubscriber();
  const nodePositions = useNodePositions();

  const height = givenHeight ?? defaultHeight ?? 0;
  const shape = givenShape ?? defaultShape ?? 'oval';
  const width = givenWidth ?? defaultWidth ?? 0;

  /*
    necessary for those who wrap this with components like <Draggable />
    see the following:
    https://github.com/mzabriskie/react-draggable/issues/414
  */
  const {
    x,
    y
  } = useMemo(
    () => {
      if (!(givenX || givenY)) {
        // eslint-disable-next-line regexp/no-unused-capturing-group
        const getNumber = /(-?\d+(\.\d+)?)/gu;
        const [
          translateX,
          translateY
        ] = nodeStyle?.transform ?
          nodeStyle.transform.match(getNumber) ?? [
            undefined,
            undefined
          ] :
          [
            undefined,
            undefined
          ];
        const left = nodeStyle?.left ?? undefined;
        const top = nodeStyle?.top ?? undefined;

        return {
          x: Number(translateX) || Number(left) || 0,
          y: Number(translateY) || Number(top) || 0
        };
      }

      return {
        x: givenX,
        y: givenY
      };
    },
    [
      givenX,
      givenY,
      nodeStyle
    ]
  );

  useEffect(
    () => {
      nodePositions?.register({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });

      return (): void => {
        nodePositions?.unregister(nodeId);
      };
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );

  useEffect(
    () => {
      nodePositions?.update({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });
    },
    [
      height,
      nodeId,
      nodePositions,
      shape,
      width,
      x,
      y
    ]
  );

  useEffect(
    (): void => {
      edgeSubscriber?.update({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });
    },
    [
      edgeSubscriber,
      height,
      nodeId,
      shape,
      width,
      x,
      y
    ]
  );

  const adjustedStyle = useMemo(
    () => {
      return {
        /* stylelint-disable */
        ...nodeStyle,
        height,
        transform: nodeStyle?.transform ?? `translate(${x}px, ${y}px) translateZ(0)`,
        width
      };
    },
    [
      height,
      nodeStyle,
      width,
      x,
      y
    ]
  );

  const className = useMemo(
    () => {
      return `${
        givenClassName
      } lincle-base-node ${
        shape === 'oval' ?
          'lincle-base-node-round' :
          ''
      }`;
    },
    [
      givenClassName,
      shape
    ]
  );

  const dataTestId = useMemo(
    () => {
      return `node-${nodeId}`;
    },
    [
      nodeId
    ]
  );

  return (
    <div
      className={className}
      data-testid={dataTestId}
      ref={ref}
      role='none'
      style={adjustedStyle}
      {...additionalProperties}
    >
      {children}
    </div>
  );
};

Node.displayName = 'LincleNode';

const refNode = forwardRef(Node);

export {
  refNode as Node
};
