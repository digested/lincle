import {
  generateOrigins,
  type PathProps,
  useDiagramId,
  useUpdateEdge
} from '../../../../shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const Path: FunctionComponent<PathProps> = ({
  center = true,
  className = '',
  edgeId,
  line,
  markerEnd: givenMarkerEnd,
  markerStart: givenMarkerStart,
  source,
  style: pathStyle = {},
  target
}) => {
  const diagramId = useDiagramId();
  const updateEdge = useUpdateEdge();

  const origins = useMemo(
    () => {
      if (
        source &&
        target &&
        line
      ) {
        const generatedOrigins = generateOrigins(
          source,
          target,
          line,
          center
        );

        return generatedOrigins;
      }

      return undefined;
    },
    [
      center,
      line,
      source,
      target
    ]
  );

  useEffect(
    () => {
      if (
        origins?.center &&
        updateEdge
      ) {
        updateEdge(
          edgeId,
          {
            center: origins.center
          }
        );
      }
    },
    [
      edgeId,
      origins?.center,
      updateEdge
    ]
  );

  const markerEnd = useMemo(
    () => {
      const id = diagramId ?
        `${diagramId}-` :
        '';

      return givenMarkerEnd ??
        `url(#diagram-${id}marker-arrow)`;
    },
    [
      diagramId,
      givenMarkerEnd
    ]
  );

  const markerStart = useMemo(
    () => {
      const id = diagramId ?
        `${diagramId}-` :
        '';

      return givenMarkerStart ??
        `url(#diagram-${id}marker-circle)`;
    },
    [
      diagramId,
      givenMarkerStart
    ]
  );

  return (
    <g>
      <path
        className='lincle-base-edge-path-border'
        d={origins?.path}
        fill='transparent'
      />
      <path
        className={`${className} lincle-base-edge-path`}
        d={origins?.path}
        fill='transparent'
        markerEnd={markerEnd}
        markerStart={markerStart}
        style={pathStyle}
      />
    </g>
  );
};

Path.displayName = 'LinclePath';

export default Path;
