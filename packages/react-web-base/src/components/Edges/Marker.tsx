import {
  useDiagramId
} from '../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const Marker: FunctionComponent<object> = () => {
  const id = useDiagramId();

  const markerId = useMemo(
    () => {
      return id ?
        `${id}-` :
        '';
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const arrowId = useMemo(
    () => {
      return `diagram-${markerId}marker-arrow`;
    },
    [
      markerId
    ]
  );

  const circleId = useMemo(
    () => {
      return `diagram-${markerId}marker-circle`;
    },
    [
      markerId
    ]
  );

  return (
    <>
      <marker
        id={arrowId}
        markerHeight='10'
        markerUnits='strokeWidth'
        markerWidth='10'
        orient='auto'
        refX='9'
        refY='5'
      >
        <path
          className='lincle-base-edge-marker-arrow'
          d='M 0 0 L 10 5 L 0 10 z'
        />
      </marker>
      <marker
        id={circleId}
        markerHeight='10'
        markerUnits='strokeWidth'
        markerWidth='10'
        orient='auto'
        refX='5'
        refY='5'
      >
        <circle
          className='lincle-base-edge-marker-circle'
          cx='5'
          cy='5'
          r='3'
        />
      </marker>
    </>
  );
};

Marker.displayName = 'LincleMarker';

export default Marker;
