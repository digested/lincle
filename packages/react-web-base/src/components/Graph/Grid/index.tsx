import {
  type GridProps,
  useDiagramId,
  useGrid
} from '../../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const SIZE = 0.4;

const Circle =
  <circle
    cx={SIZE}
    cy={SIZE}
    fill='black'
    r={SIZE}
  />;
const Grid: FunctionComponent<GridProps> = (
  {
    children,
    scale = 1,
    xOffset: givenXOffset = 0,
    yOffset: givenYOffset = 0
  }: GridProps
) => {
  const diagramId = useDiagramId();
  const [
    givenGridX,
    givenGridY
  ] = useGrid() || [
    1,
    1
  ];

  const id = `lincle-grid-${diagramId}`;

  const gridX = useMemo(
    () => {
      return givenGridX * scale;
    },
    [
      givenGridX,
      scale
    ]
  );

  const gridY = useMemo(
    () => {
      return givenGridY * scale;
    },
    [
      givenGridY,
      scale
    ]
  );

  const xOffset = useMemo(
    () => {
      return givenXOffset % gridX;
    },
    [
      givenXOffset,
      gridX
    ]
  );

  const yOffset = useMemo(
    () => {
      return givenYOffset % gridY;
    },
    [
      givenYOffset,
      gridY
    ]
  );

  return (
    <svg
      className='lincle-base-grid'
    >
      <pattern
        height={gridY}
        id={id}
        patternUnits='userSpaceOnUse'
        width={gridX}
        x={xOffset}
        y={yOffset}
      >
        {children ?? Circle}
      </pattern>
      <rect
        fill={`url(#${id})`}
        height='100%'
        width='100%'
        x='0'
        y='0'
      />
    </svg>
  );
};

Grid.displayName = 'LincleGrid';

export default Grid;
