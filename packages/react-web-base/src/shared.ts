import {
  type EdgesProps as EdgesPartialProps,
  type EdgeProps as SharedEdgeProps,
  type NodeProps as SharedNodeProps,
  type PathProps as SharedPathProps
} from '@lincle/react-shared';
import {
  type CSSProperties
} from 'react';

export type EdgeProps = SharedEdgeProps & {
  className?: string;
  style?: {
    edge?: CSSProperties;
  };
};

export type EdgesProps = EdgesPartialProps & {
  readonly className?: string;
  readonly style?: CSSProperties;
};

export type NodeProps = SharedNodeProps & {
  className?: string;
  style?: {
    node?: CSSProperties;
  };
};

export type PathProps = SharedPathProps & {
  className?: string;
  style?: CSSProperties;
};

export {
  type EdgeNodeProps,
  generateOrigins,
  type GraphProps,
  type GridProps,
  type Line,
  type NodesProps,
  Providers,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useDiagramId,
  useEdge,
  useEdges,
  useEdgeSubscriber,
  useGrid,
  useNodePositions,
  useRemoveEdge,
  useUpdateEdge
} from '@lincle/react-shared';
