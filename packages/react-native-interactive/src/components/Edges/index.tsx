import {
  Edges as BaseEdges
} from '../../base';
import {
  type EdgesProps,
  useScale,
  useTranslate
} from '../../shared';
import Connections from './Connections';
import {
  type FunctionComponent
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const {
  edgesStyle
} = StyleSheet.create({
  edgesStyle: {
    height: '100%',
    pointerEvents: 'box-none',
    position: 'absolute',
    width: '100%'
  }
});

const Edges: FunctionComponent<EdgesProps> = ({
  children,
  ...props
}) => {
  const scale = useScale();
  const translate = useTranslate();

  return (
    <View
      style={edgesStyle}
    >
      <BaseEdges
        scale={scale}
        translate={translate}
        {...props}
      >
        {children}
      </BaseEdges>
      <Connections
        scale={scale}
        translate={translate}
      />
    </View>
  );
};

Edges.displayName = 'LincleInteractiveEdges';

export {
  Edges
};
