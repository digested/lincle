import {
  Node as WrappedNode
} from '../../../base';
import {
  type MoveNodeProps,
  useScale
} from '../../../shared';
import Draggable from '../Draggable';
import {
  type FunctionComponent,
  useCallback,
  useMemo,
  useState
} from 'react';
import {
  type GestureResponderEvent,
  type PanResponderGestureState
} from 'react-native';

const MoveNode: FunctionComponent<MoveNodeProps> = ({
  children,
  disabled,
  height = 50,
  id,
  // mode,
  onDrag,
  // onSelect,
  // onStart,
  // onStop,
  shape,
  style: {
    node: nodeStyle
  } = {},
  // snap,
  track = false,
  width = 50,
  x = 0,
  y = 0
}) => {
  const scale = useScale();

  const [
    position,
    setPosition
  ] = useState({
    x,
    y
  });

  const [
    finalPosition,
    setFinalPosition
  ] = useState({
    x,
    y
  });

  const handleDrag = useCallback(
    (
      event: GestureResponderEvent,
      state: PanResponderGestureState
    ) => {
      if (onDrag) {
        onDrag(
          event as GestureResponderEvent,
          {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            deltaX: state.dx,
            deltaY: state.dy,
            lastX: finalPosition.x,
            lastY: finalPosition.y,
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            node: event.target.context as Element,
            x: finalPosition.x + state.dx,
            y: finalPosition.y + state.dy
          },
          {
            x: finalPosition.x + state.dx,
            y: finalPosition.y + state.dy
          }
        );
      }

      setPosition(
        () => {
          return {
            x: finalPosition.x + state.dx,
            y: finalPosition.y + state.dy
            // x: state.moveX,
            // y: state.moveY
          };
        }
      );
    },
    [
      finalPosition.x,
      finalPosition.y,
      onDrag
    ]
  );

  const handleDragRelease = useCallback(
    (
      event: GestureResponderEvent,
      state: PanResponderGestureState,
      bounds: {
        bottom: number;
        left: number;
        right: number;
        top: number;
      }
    ) => {
      setFinalPosition(
        () => {
          return {
            x: bounds.left,
            y: bounds.top
            // x: state.moveX,
            // y: state.moveY
          };
        }
      );
    },
    []
  );

  const style = useMemo(
    () => {
      return {
        node: nodeStyle
      };
    },
    [
      nodeStyle
    ]
  );

  return (
    <Draggable
      disabled={disabled}
      onDrag={handleDrag}
      onDragRelease={handleDragRelease}
      scale={scale}
      x={x}
      y={y}
    >
      <WrappedNode
        height={height}
        id={id}
        shape={shape}
        style={style}
        track={track}
        width={width}
        x={position.x}
        y={position.y}
      >
        {children}
      </WrappedNode>
    </Draggable>
  );
};

MoveNode.displayName = 'LincleInteractiveMoveNode';

export default MoveNode;
