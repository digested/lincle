/**
 * https://github.com/tongyy/react-native-draggable
 */

import React, {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  Animated,
  Dimensions,
  type GestureResponderEvent,
  Image,
  type LayoutChangeEvent,
  PanResponder,
  type PanResponderGestureState,
  type StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  type ViewStyle
} from 'react-native';

const clamp = (number: number, min: number, max: number) => {
  return Math.max(
    min,
    Math.min(
      number,
      max
    )
  );
};

const styles = StyleSheet.create({
  debugView: {
    backgroundColor: '#ff000044',
    borderColor: '#fced0ecc',
    borderWidth: 4,
    position: 'absolute'
  },
  text: {
    color: '#fff',
    textAlign: 'center'
  }
});

const Draggable: FunctionComponent<IProps> = ({
  animatedViewProps,
  children,
  debug = false,
  disabled = false,
  imageSource,
  isCircle,
  maxX,
  maxY,
  minX,
  minY,
  onDrag,
  onDragRelease,
  onLongPress,
  onPressIn,
  onPressOut,
  onRelease,
  onShortPressRelease,
  renderColor,
  renderSize = 36,
  renderText = '＋',
  scale = 1,
  shouldReverse = false,
  touchableOpacityProps,
  x = 0,
  y = 0,
  zIndex = 1
}) => {
  // The Animated object housing our xy value so that we can spring back
  const pan = useRef(new Animated.ValueXY());

  // Always set to xy value of pan, would like to remove
  const offsetFromStart = useRef({
    x: 0,
    y: 0
  });

  // Width/Height of Draggable (renderSize is arbitrary if children are passed in)
  const childSize = useRef({
    x: renderSize,
    y: renderSize
  });

  // Top/Left/Right/Bottom location on screen from start of most recent touch
  const startBounds = useRef({
    bottom: 0,
    left: 0,
    right: 0,
    top: 0
  });

  // Whether we're currently dragging or not
  const isDragging = useRef(false);

  const getBounds = useCallback(
    () => {
      const left = x + offsetFromStart.current.x;
      const top = y + offsetFromStart.current.y;
      return {
        bottom: top + childSize.current.y,
        left,
        right: left + childSize.current.x,
        top
      };
    },
    [
      x,
      y
    ]
  );

  const shouldStartDrag = useCallback(
    (
      gs: {
        dx: number;
        dy: number;
      }
    ) => {
      return !disabled && (Math.abs(gs.dx) > 2 || Math.abs(gs.dy) > 2);
    },
    [
      disabled
    ]
  );

  const reversePosition = useCallback(
    () => {
      Animated.spring(
        pan.current,
        {
          toValue: {
            x: 0,
            y: 0
          },
          useNativeDriver: false
        }
      ).start();
    },
    [
      pan
    ]
  );

  const onPanResponderRelease = useCallback(
    (
      event: GestureResponderEvent,
      gestureState: PanResponderGestureState
    ) => {
      isDragging.current = false;

      if (onDragRelease) {
        onDragRelease(
          event,
          gestureState,
          getBounds()
        );

        if (onRelease) {
          onRelease(
            event,
            true
          );
        }
      }

      if (shouldReverse) {
        reversePosition();
      } else {
        pan.current.flattenOffset();
      }
    },
    [
      getBounds,
      onDragRelease,
      onRelease,
      reversePosition,
      shouldReverse
    ]
  );

  const onPanResponderGrant = useCallback(
    (
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      event: GestureResponderEvent,
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      gestureState: PanResponderGestureState
    ) => {
      startBounds.current = getBounds();
      isDragging.current = true;

      if (!shouldReverse) {
        pan.current.setOffset(offsetFromStart.current);
        pan.current.setValue({
          x: 0,
          y: 0
        });
      }
    },
    [
      getBounds,
      shouldReverse
    ]
  );

  const handleOnDrag = useCallback(
    (
      event: GestureResponderEvent,
      gestureState: PanResponderGestureState
    ) => {
      const {
        dx,
        dy
      } = gestureState;

      const {
        bottom,
        left,
        right,
        top
      } = startBounds.current;

      const far = 999_999_999;

      const changeX = clamp(
        dx,
        Number.isFinite(minX) ?
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
          minX - left :
          -far,
        Number.isFinite(maxX) ?
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
          maxX - right :
          far
      );
      const changeY = clamp(
        dy,
        Number.isFinite(minY) ?
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
          minY - top :
          -far,
        Number.isFinite(maxY) ?
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
          maxY - bottom :
          far
      );

      pan.current.setValue({
        x: changeX / scale,
        y: changeY / scale
      });

      if (onDrag) {
        onDrag(
          event,
          {
            ...gestureState,
            dx: changeX / scale,
            dy: changeY / scale
          }
        );
      }
    },
    [
      maxX,
      maxY,
      minX,
      minY,
      onDrag,
      scale
    ]
  );

  const panResponder = useMemo(
    () => {
      return PanResponder.create({
        onMoveShouldSetPanResponder: (_, gestureState) => {
          return shouldStartDrag(gestureState);
        },
        onMoveShouldSetPanResponderCapture: (_, gestureState) => {
          return shouldStartDrag(gestureState);
        },
        onPanResponderGrant,
        onPanResponderMove: Animated.event(
          [],
          {
            // Typed incorrectly https://reactnative.dev/docs/panresponder
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            listener: handleOnDrag,
            useNativeDriver: false
          }
        ),
        onPanResponderRelease
      });
    },
    [
      handleOnDrag,
      onPanResponderGrant,
      onPanResponderRelease,
      shouldStartDrag
    ]
  );

  /**
   * !TODO Figure out a way to destroy and remove offsetFromStart entirely
   */
  useEffect(
    () => {
      // Using an instance to avoid losing the pointer before the cleanup
      const currentPan = pan.current;

      if (!shouldReverse) {
        currentPan.addListener(
          (cp) => {
            offsetFromStart.current = cp;

            return offsetFromStart.current;
          }
        );
      }

      return () => {
        // Typed incorrectly
        currentPan.removeAllListeners();
      };
    },
    [
      shouldReverse
    ]
  );

  const positionCss: StyleProp<ViewStyle> = useMemo(
    () => {
      const Window = Dimensions.get('window');

      return {
        height: Window.height,
        left: 0,
        position: 'absolute',
        top: 0,
        width: Window.width
      };
    },
    []
  );

  const hasChildren = Boolean(children);

  const dragItemCss = useMemo<StyleProp<ViewStyle>>(
    () => {
      const style: StyleProp<ViewStyle> = {
        elevation: zIndex,
        left: x,
        top: y,
        zIndex
      };

      if (renderColor) {
        style.backgroundColor = renderColor;
      }

      if (isCircle) {
        style.borderRadius = renderSize;
      }

      if (hasChildren) {
        style.alignSelf = 'baseline';

        return style;
      } else {
        style.height = renderSize;
        style.justifyContent = 'center';
        style.width = renderSize;

        return style;
      }
    },
    [
      hasChildren,
      isCircle,
      renderColor,
      renderSize,
      x,
      y,
      zIndex
    ]
  );

  const touchableContent = useMemo(
    () => {
      if (imageSource) {
        const style = {
          height: renderSize,
          width: renderSize
        };

        return (
          <Image
            source={imageSource}
            style={style}
          />
        );
      } else {
        return (
          <Text
            style={styles.text}
          >
            {renderText}
          </Text>
        );
      }
    },
    [
      imageSource,
      renderSize,
      renderText
    ]
  );

  const handleOnLayout = useCallback(
    (event: LayoutChangeEvent) => {
      const {
        height,
        width
      } = event.nativeEvent.layout;
      childSize.current = {
        x: width,
        y: height
      };
    },
    []
  );

  const handlePressOut = useCallback(
    (event: GestureResponderEvent) => {
      if (onPressOut) {
        onPressOut(event);
      }

      if (
        !isDragging.current &&
        onRelease
      ) {
        onRelease(
          event,
          false
        );
      }
    },
    [
      onPressOut,
      onRelease
    ]
  );

  const getDebugView = useCallback(
    () => {
      const {
        height,
        width
      } = Dimensions.get('window');
      const far = 9_999;
      const constrained = minX ?? maxX ?? minY ?? maxY;
      if (!constrained) {
        return null;
      }
      // could show other debug info here

      const left = minX ?? -far;
      const right = maxX ?
        width - maxX :
        -far;
      const top = minY ?? -far;
      const bottom = maxY ?
        height - maxY :
        -far;

      const style = {
        bottom,
        left,
        right,
        top,
        ...styles.debugView
      };

      return (
        <View
          pointerEvents='box-none'
          style={style}
        />
      );
    },
    [
      maxX,
      maxY,
      minX,
      minY
    ]
  );

  return (
    <View pointerEvents='box-none' style={positionCss}>
      {debug && getDebugView()}
      <Animated.View
        pointerEvents='box-none'
        {...animatedViewProps}
        {...panResponder.panHandlers}
        style={pan.current.getLayout()}
      >
        <TouchableOpacity
          {...touchableOpacityProps}
          activeOpacity={1}
          disabled={disabled}
          onLayout={handleOnLayout}
          onLongPress={onLongPress}
          onPress={onShortPressRelease}
          onPressIn={onPressIn}
          onPressOut={handlePressOut}
          style={dragItemCss}
        >
          {children ?? touchableContent}
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
};

Draggable.displayName = 'ReactNativeDraggable';

export default Draggable;

type IProps = {
  readonly animatedViewProps?: object;
  /**
   *
   */
  readonly children?: React.ReactNode;
  readonly debug?: boolean;
  readonly disabled?: boolean;
  readonly imageSource?: number;
  readonly isCircle?: boolean;
  readonly maxX?: number;
  readonly maxY?: number;
  readonly minX?: number;
  readonly minY?: number;
  readonly onDrag?: (
    event: GestureResponderEvent,
    gestureState: PanResponderGestureState
  ) => void;
  readonly onDragRelease?: (
    event: GestureResponderEvent,
    gestureState: PanResponderGestureState,
    bounds: {
      bottom: number;
      left: number;
      right: number;
      top: number;
    }
  ) => void;
  readonly onLongPress?: (
    event: GestureResponderEvent
  ) => void;
  readonly onPressIn?: (
    event: GestureResponderEvent
  ) => void;
  readonly onPressOut?: (
    event: GestureResponderEvent
  ) => void;
  readonly onRelease?: (
    event: GestureResponderEvent,
    wasDragging: boolean
  ) => void;
  readonly onReverse?: () => {
    x: number;
    y: number;
  };
  readonly onShortPressRelease?: (
    event: GestureResponderEvent
  ) => void;
  readonly renderColor?: string;
  readonly renderSize?: number;
  /**
   * ** props that should probably be removed in favor of "children"
   */
  readonly renderText?: string;
  readonly scale?: number;
  readonly shouldReverse?: boolean;
  readonly touchableOpacityProps?: object;
  readonly x?: number;
  readonly y?: number;
  // z/elevation should be removed because it doesn't sync up visually and haptically
  readonly zIndex?: number;
};
