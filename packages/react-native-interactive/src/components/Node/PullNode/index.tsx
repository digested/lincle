import {
  type DraggableData,
  type DraggableEvent,
  type PullNodeProps,
  useDiagramId,
  useNodePositions,
  useOnNodeEdgeDrop,
  useScale,
  useSetConnection
} from '../../../shared';
import Draggable from '../Draggable';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const PullNode: FunctionComponent<PullNodeProps> = ({
  disabled,
  height = 0,
  line = 'direct',
  onDrag,
  onEdgeDrop: givenOnEdgeDrop,
  onStart,
  onStop,
  shape = 'oval',
  sourceId,
  style: givenStyle,
  width = 0,
  x = 0,
  y = 0
}) => {
  const diagramId = useDiagramId();
  const nodePositions = useNodePositions();
  const setConnection = useSetConnection(sourceId as string);
  const onNodeEdgeDrop = useOnNodeEdgeDrop();
  const onEdgeDrop = givenOnEdgeDrop ?? onNodeEdgeDrop;
  const scale = useScale();

  const mouseOffset = useRef<null | {
    x: number;
    y: number;
  }>(null);

  const position = useRef<null | {
    x: number;
    y: number;
  }>(null);

  useEffect(
    () => {
      return () => {
        setConnection();
        position.current = null;
        mouseOffset.current = null;
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const handlePressIn = useCallback(
    (
      event: DraggableEvent
    ) => {
      event.stopPropagation();

      if (
        !disabled &&
        onStart
      ) {
        onStart(
          event
        );
      }
    },
    [
      disabled,
      onStart
    ]
  );

  const handleDrag = useCallback(
    (
      event: DraggableEvent,
      gestureState: DraggableData
    ) => {
      if (!mouseOffset.current) {
        mouseOffset.current = {
          x: event.nativeEvent.locationX,
          y: event.nativeEvent.locationY
        };
      }

      if (!disabled) {
        position.current = {
          x: x + gestureState.dx + mouseOffset.current?.x,
          y: y + gestureState.dy + mouseOffset.current?.y
        };

        setConnection({
          line,
          source: {
            height,
            id: sourceId,
            shape,
            width,
            x,
            y
          },
          target: {
            height: 0,
            id: sourceId,
            shape,
            width: 0,
            x: position.current.x,
            y: position.current.y
          }
        });

        if (onDrag) {
          onDrag(
            event,
            gestureState
          );
        }
      }
    },
    [
      disabled,
      height,
      line,
      onDrag,
      setConnection,
      shape,
      sourceId,
      width,
      x,
      y
    ]
  );

  const handleRelease = useCallback(
    (
      event: DraggableEvent,
      data: DraggableData
    ) => {
      setConnection();

      if (onStop) {
        onStop(
          event,
          data
        );
      }

      if (
        onEdgeDrop &&
        nodePositions &&
        position.current
      ) {
        nodePositions.match(
          position.current.x,
          position.current.y
          // eslint-disable-next-line promise/prefer-await-to-then
        ).then(
          (match) => {
            onEdgeDrop(
              event,
              sourceId,
              Boolean(match) || match === 0 ?
                match :
                undefined
            );
          }
          // eslint-disable-next-line promise/prefer-await-to-then
        ).catch(
          () => {
            //
          }
        );
      }

      position.current = null;
      mouseOffset.current = null;
    },
    [
      nodePositions,
      onEdgeDrop,
      onStop,
      setConnection,
      sourceId
    ]
  );

  const style = useMemo(
    () => {
      return StyleSheet.create({
        view: {
          ...givenStyle,
          borderRadius: shape === 'oval' ?
            50 :
            undefined,
          height,
          width
        }
      });
    },
    [
      givenStyle,
      height,
      shape,
      width
    ]
  );

  return diagramId ?

    <Draggable
      disabled={disabled}
      onDrag={handleDrag}
      onDragRelease={handleRelease}
      onPressIn={handlePressIn}
      renderSize={height}
      scale={scale}
      shouldReverse
      x={x}
      y={y}
      // isCircle={shape === 'oval'}
    >
      <View
        style={style.view}
      />
    </Draggable> :
    null;
};

PullNode.displayName = 'LincleInteractivePullNode';

export default PullNode;
