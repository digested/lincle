import {
  GraphBase,
  type GraphProps,
  Providers
} from '../../shared';
import Interaction from '../Interaction';
import Grid from './Grid';
import {
  type FunctionComponent
} from 'react';

const Graph: FunctionComponent<GraphProps> = ({
  children,
  edgeFrequency,
  grid,
  id,
  line,
  maxScale,
  minScale,
  mode,
  move,
  nodeFrequency,
  nodeHeight,
  nodeWidth,
  onNodeDrag,
  onNodeEdgeDrop,
  onNodeSelect,
  onNodeStart,
  onNodeStop,
  onScale,
  onTranslate,
  pan,
  pull,
  scale,
  shape,
  showGrid,
  snap,
  translate,
  zoom
}) => {
  if (
    !id
  ) {
    // eslint-disable-next-line no-console
    console.error('No ID provided to @lincle/interactive Graph!');

    return null;
  }

  const gird = showGrid === false ?
    null :
    <Grid />;

  return (
    <GraphBase
      edgeFrequency={edgeFrequency}
      grid={grid}
      id={id}
      line={line}
      nodeFrequency={nodeFrequency}
      nodeHeight={nodeHeight}
      nodeWidth={nodeWidth}
      shape={shape}
      showGrid={false}
    >
      <Providers
        maxScale={maxScale}
        minScale={minScale}
        mode={mode}
        move={move}
        onNodeDrag={onNodeDrag}
        onNodeEdgeDrop={onNodeEdgeDrop}
        onNodeSelect={onNodeSelect}
        onNodeStart={onNodeStart}
        onNodeStop={onNodeStop}
        onScale={onScale}
        onTranslate={onTranslate}
        pan={pan}
        pull={pull}
        scale={scale}
        snap={snap}
        translate={translate}
        zoom={zoom}
      >
        {gird}
        <Interaction />
        {children}
      </Providers>
    </GraphBase>
  );
};

Graph.displayName = 'LincleInteractiveGraph';

export {
  Graph
};

export {
  default as Grid
} from './Grid';
