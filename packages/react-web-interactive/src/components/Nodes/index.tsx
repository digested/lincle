import {
  Nodes as BaseNodes
} from '../../base';
import {
  type NodesProps
} from '../../shared';
import Interaction from './Interaction';
import {
  type FunctionComponent
} from 'react';

const Nodes: FunctionComponent<NodesProps> = ({
  children,
  ...props
}) => {
  return (
    <Interaction>
      <BaseNodes
        {...props}
      >
        {children}
      </BaseNodes>
    </Interaction>
  );
};

Nodes.displayName = 'LincleInteractiveNodes';

export {
  Nodes
};
