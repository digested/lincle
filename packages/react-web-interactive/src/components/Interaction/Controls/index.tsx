import {
  type InteractionControlProps,
  useMode,
  useOnMode
} from '../../../shared';
import {
  type FunctionComponent,
  type MouseEvent,
  type TouchEvent,
  useCallback,
  useEffect,
  useRef
} from 'react';

const CLICK_TIME = 500;

const useDoubleTap = () => {
  const timer = useRef<NodeJS.Timeout | null>(null);

  useEffect(
    () => {
      return () => {
        if (timer.current) {
          clearTimeout(timer.current);
          timer.current = null;
        }
      };
    },
    []
  );

  return useCallback(
    (
      callback: () => void,
      threshold = CLICK_TIME
    ) => {
      if (timer.current) {
        clearTimeout(timer.current);
        timer.current = null;

        // eslint-disable-next-line n/callback-return
        callback();
      } else {
        timer.current = setTimeout(
          () => {
            timer.current = null;
          },
          threshold
        );

        // eslint-disable-next-line no-useless-return
        return;
      }
    },
    []
  );
};

const Controls: FunctionComponent<InteractionControlProps> = ({
  onMouseDown,
  onMouseUp,
  onTouchEnd,
  onTouchStart
}) => {
  const graphMode = useMode() ?? 'move';
  const onMode = useOnMode();

  const pointerPosition = useRef<{
    [eventId: string]: {
      x: number;
      y: number;
    };
  }>(
    {
      mouse: {
        x: 0,
        y: 0
      }
    }
  );

  const handleDoubleTap = useDoubleTap();

  const handleTapEnd = useCallback(
    () => {
      if (onMode) {
        switch (graphMode) {
          case 'move': {
            handleDoubleTap(
              () => {
                onMode('pull');
              }
            );

            return;
          }

          case 'pull': {
            handleDoubleTap(
              () => {
                onMode('select');
              }
            );

            return;
          }

          case 'select': {
            handleDoubleTap(
              () => {
                onMode('move');
              }
            );
          }
        }
      }
    },
    [
      graphMode,
      handleDoubleTap,
      onMode
    ]
  );

  const handleMouseDown = useCallback(
    (
      event: MouseEvent<HTMLDivElement>
    ) => {
      pointerPosition.current = {
        mouse: {
          x: event.clientX,
          y: event.clientY
        }
      };

      if (onMouseDown) {
        onMouseDown(event);
      }
    },
    [
      onMouseDown
    ]
  );

  const handleMouseUp = useCallback(
    (
      event: MouseEvent<HTMLDivElement>
    ) => {
      if (
        pointerPosition.current.mouse.x === Math.round(event.clientX) &&
        pointerPosition.current.mouse.y === Math.round(event.clientY)
      ) {
        handleTapEnd();
      }

      if (onMouseUp) {
        onMouseUp(event);
      }
    },
    [
      handleTapEnd,
      onMouseUp
    ]
  );

  const handleTouchStart = useCallback(
    (
      event: TouchEvent<HTMLDivElement>
    ) => {
      for (
        const [
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          index,
          touch
        ] of Object.entries(event.changedTouches)
      ) {
        pointerPosition.current[touch.identifier] = {
          x: touch.clientX,
          y: touch.clientY
        };
      }

      if (onTouchStart) {
        onTouchStart(event);
      }
    },
    [
      onTouchStart
    ]
  );

  const handleTouchEnd = useCallback(
    (
      event: TouchEvent<HTMLDivElement>
    ) => {
      let shouldHandleTapEnd = false;

      for (
        const [
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          index,
          touch
        ] of Object.entries(event.changedTouches)
      ) {
        if (
          pointerPosition.current[touch.identifier].x === touch.clientX &&
          pointerPosition.current[touch.identifier].y === touch.clientY
        ) {
          shouldHandleTapEnd = true;
        }
      }

      if (
        shouldHandleTapEnd
      ) {
        handleTapEnd();
      }

      if (onTouchEnd) {
        onTouchEnd(event);
      }
    },
    [
      handleTapEnd,
      onTouchEnd
    ]
  );

  return (
    <div
      className='lincle-interactive-container'
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onTouchEnd={handleTouchEnd}
      onTouchStart={handleTouchStart}
      role='none'
    />
  );
};

Controls.displayName = 'LincleInteractiveControls';

export default Controls;
