import {
  Node
} from '../../../base';
import {
  type DraggableEvent,
  type MoveNodeProps,
  useScale,
  useSnap
} from '../../../shared';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import {
  DraggableCore,
  type DraggableData as DraggableD,
  type DraggableEvent as DraggableE,
  type DraggableEventHandler
} from 'react-draggable';

const PRESS_TIME = 500;
const emptySnap = [
  1,
  1
];

const MoveNode: FunctionComponent<MoveNodeProps> = ({
  children,
  className,
  disabled,
  height,
  id,
  mode,
  onDrag,
  onSelect,
  onStart,
  onStop,
  shape,
  snap: givenSnap,
  style,
  width,
  x,
  y
}) => {
  const scale = useScale();
  const snap = useSnap();

  const [
    snapX,
    snapY
  ] = useMemo(
    () => {
      if (
        givenSnap === undefined
      ) {
        if (Array.isArray(snap)) {
          return snap;
        } else {
          return emptySnap;
        }
      } else if (
        Array.isArray(givenSnap)
      ) {
        return givenSnap;
      } else {
        return emptySnap;
      }
    },
    [
      givenSnap,
      snap
    ]
  );

  const [
    position,
    setPosition
  ] = useState({
    x,
    y
  });
  const mouseDown = useRef(false);
  const initialPos = useRef(position);

  if (
    !mouseDown.current && (
      x !== initialPos.current.x ||
      y !== initialPos.current.y
    ) && (
      x !== position.x ||
      y !== position.y
    )
  ) {
    setPosition({
      x,
      y
    });
  }

  const pos = useRef(position);
  const snapped = useRef(position);
  snapped.current = position;

  const moved = useRef(false);
  const press = useRef<NodeJS.Timeout>(undefined);

  useEffect(
    () => {
      return (): void => {
        if (press.current) {
          clearTimeout(press.current);
        }
      };
    },
    []
  );

  const handleOnSelect = useCallback(
    () => {
      if (onSelect) {
        onSelect(id);
      }
    },
    [
      id,
      onSelect
    ]
  );

  const handlePressStart = useCallback(
    () => {
      if (
        mode !== 'select' &&
        !press.current
      ) {
        press.current = setTimeout(
          (): void => {
            handleOnSelect();
            press.current = undefined;
          },
          PRESS_TIME
        );
      }
    },
    [
      handleOnSelect,
      mode
    ]
  );

  const handlePressStop = useCallback(
    () => {
      if (press.current) {
        clearTimeout(press.current);
        press.current = undefined;
      }
    },
    []
  );

  const handleStart = useCallback<DraggableEventHandler>(
    (
      event: DraggableE
    ) => {
      event.stopPropagation();

      mouseDown.current = true;

      if (onSelect) {
        handlePressStart();
      }

      if (onStart) {
        onStart(event as DraggableEvent);
      }
    },
    [
      handlePressStart,
      onSelect,
      onStart
    ]
  );

  const handleDrag = useCallback<DraggableEventHandler>(
    (
      event: DraggableE,
      data: DraggableD
    ) => {
      event.stopPropagation();

      const currentX = pos.current.x;
      const currentY = pos.current.y;

      if (
        !disabled &&
        (currentX || currentX === 0) &&
        (currentY || currentY === 0)
      ) {
        if (onSelect) {
          handlePressStop();
        }

        pos.current = {
          x: currentX + data.deltaX,
          y: currentY + data.deltaY
        };

        const adjustedPosition = {
          x: Math.floor((currentX + snapX / 2) / snapX) * snapX,
          y: Math.floor((currentY + snapY / 2) / snapY) * snapY
        };

        setPosition(adjustedPosition);

        if (onDrag) {
          onDrag(
            event as DraggableEvent,
            {
              ...data,
              ...adjustedPosition
            },
            snapped.current
          );
        }

        if (moved.current === false) {
          moved.current = true;
        }
      }
    },
    [
      disabled,
      handlePressStop,
      onDrag,
      onSelect,
      snapX,
      snapY
    ]
  );

  const handleStop = useCallback<DraggableEventHandler>(
    (
      event: DraggableE,
      data: DraggableD
    ) => {
      mouseDown.current = false;

      if (
        onSelect && !press.current ||
          mode === 'select' ||
          moved.current
      ) {
        event.stopPropagation();
      }

      if (!disabled) {
        if (
          moved.current === false &&
          mode === 'select'
        ) {
          handleOnSelect();
        }

        if (onSelect) {
          handlePressStop();
        }

        const addX = snapped.current.x || snapped.current.x === 0 ?
          snapped.current.x + data.deltaX :
          0;
        const addY = snapped.current.y || snapped.current.y === 0 ?
          snapped.current.y + data.deltaY :
          0;

        const adjustedPosition = {
          x: addX - addX % snapX,
          y: addY - addY % snapY
        };

        setPosition({
          x: adjustedPosition.x,
          y: adjustedPosition.y
        });

        if (onStop) {
          onStop(
            event as DraggableEvent,
            {
              ...data,
              ...adjustedPosition
            }
          );
        }

        moved.current = false;
      }
    },
    [
      disabled,
      handleOnSelect,
      handlePressStop,
      mode,
      onSelect,
      onStop,
      snapX,
      snapY
    ]
  );

  const nodeRef = useRef(null);

  return (
    <DraggableCore
      disabled={disabled}
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      nodeRef={nodeRef}
      onDrag={handleDrag}
      onStart={handleStart}
      onStop={handleStop}
      scale={scale}
    >
      <Node
        className={className}
        height={height}
        id={id}
        ref={nodeRef}
        shape={shape}
        style={style}
        width={width}
        x={position.x}
        y={position.y}
      >
        {children}
      </Node>
    </DraggableCore>
  );
};

MoveNode.displayName = 'LincleInteractiveMoveNode';

export default MoveNode;
