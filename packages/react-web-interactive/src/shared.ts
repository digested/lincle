import {
  type DraggableData,
  type EdgeProps,
  type GraphProps as InteractiveGraphProps,
  type Position,
  type MoveNodeProps as SharedMoveNodeProps,
  type NodeProps as SharedNodeProps,
  type PullNodeProps as SharedPullNodeProps,
  type PullProps as SharedPullProps
} from '@lincle/react-interactive-shared';
import {
  type NodeProps as BaseNodeProps
} from '@lincle/react-web-base';
import {
  type CSSProperties,
  type MouseEvent,
  type PropsWithChildren,
  type TouchEvent
} from 'react';

export type DraggableEvent = MouseEvent | MouseEvent<HTMLElement | SVGElement> | TouchEvent | TouchEvent<HTMLElement | SVGElement>;

export type GraphNodeControlProps = {
  onNodeDrag?: (
    event: DraggableEvent,
    nodeId: number | string,
    data: DraggableData
  ) => void;
  onNodeSelect?: (
    nodeId: number | string
  ) => void;
  onNodeStart?: (
    event: DraggableEvent,
    nodeId: number | string
  ) => void;
  onNodeStop?: (
    event: DraggableEvent,
    nodeId: number | string,
    data: DraggableData
  ) => void;
};

export type GraphProps = GraphNodeControlProps &
InteractionProps &
Omit<
  InteractiveGraphProps,
'onNodeDrag' |
'onNodeSelect' |
'onNodeStart' |
'onNodeStop'
>;

export type InteractionControlProps = {
  onMouseDown?: (
    event: MouseEvent<HTMLDivElement>
  ) => void;
  onMouseUp?: (
    event: MouseEvent<HTMLDivElement>
  ) => void;
  onTouchEnd?: (
    event: TouchEvent<HTMLDivElement>
  ) => void;
  onTouchStart?: (
    event: TouchEvent<HTMLDivElement>
  ) => void;
};

export type InteractionProps = PropsWithChildren<InteractionControlProps & {}>;

export type MoveNodeProps = BaseNodeProps & Omit<NodeProps, 'onDrag' | 'onSelect'> & SharedMoveNodeProps & {
  readonly onDrag?: (
    event: DraggableEvent,
    data: DraggableData,
    position: Position
  ) => void;
};

export type NodeControlProps = {
  onDrag?: (
    event: DraggableEvent,
    data: DraggableData
  ) => void;
  onEdgeDrop?: (
    event: DraggableEvent,
    sourceId: number | string,
    targetId?: number | string
  ) => void;
  onSelect?: () => void;
  onStart?: (
    event: DraggableEvent
  ) => void;
  onStop?: (
    event: DraggableEvent,
    data: DraggableData
  ) => void;
};

export type NodeProps = BaseNodeProps & NodeControlProps & SharedNodeProps;

export type PullNodeProps = Omit<NodeProps, 'id'> & SharedPullNodeProps & {
  className?: string;
  style?: {
    node?: CSSProperties;
    pull?: CSSProperties;
  };
};

export type PullProps = Omit<EdgeProps, 'children' | 'id' | 'path' | 'targetId'> &
Omit<NodeProps, 'children' | 'id'> & SharedPullProps & {
  className?: string;
  style?: {
    pull?: CSSProperties;
  };
};

export {
  type ConnectionsProps,
  type DraggableData,
  type EdgeNodeProps,
  type EdgesProps,
  type GridProps,
  type ModeType,
  type NodesProps,
  type Position,
  Providers,
  useConnections,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useDiagramId,
  useMaxScale,
  useMinScale,
  useMode,
  useModeGiven,
  useMove,
  useNodePositions,
  useOnMode,
  useOnNodeDrag,
  useOnNodeEdgeDrop,
  useOnNodeSelect,
  useOnNodeStart,
  useOnNodeStop,
  useOnScale,
  useOnTranslate,
  usePan,
  usePull,
  useScale,
  useSetConnection,
  useSnap,
  useTranslate,
  useZoom
} from '@lincle/react-interactive-shared';
export {
  Graph as GraphBase
} from '@lincle/react-web-base';
