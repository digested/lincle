import {
  NodeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnNodeStart = () => {
  const {
    onNodeStart
  } = useContext(NodeContext);

  return onNodeStart;
};

export {
  useOnNodeStart
};
