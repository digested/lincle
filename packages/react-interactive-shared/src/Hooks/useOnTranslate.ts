import {
  TranslateContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnTranslate = () => {
  const {
    onTranslate
  } = useContext(TranslateContext);

  return onTranslate;
};

export {
  useOnTranslate
};
