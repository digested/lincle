import {
  TranslateContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useTranslate = () => {
  const {
    translate
  } = useContext(TranslateContext);

  return translate;
};

export {
  useTranslate
};
