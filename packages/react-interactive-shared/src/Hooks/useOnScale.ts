import {
  ScaleContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnScale = () => {
  const {
    onScale
  } = useContext(ScaleContext);

  return onScale;
};

export {
  useOnScale
};
