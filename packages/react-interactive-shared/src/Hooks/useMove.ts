import {
  ModeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useMove = () => {
  const {
    move
  } = useContext(ModeContext);

  return move;
};

export {
  useMove
};
