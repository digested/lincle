import {
  TranslateContext
} from '../Contexts';
import {
  useContext
} from 'react';

const usePan = () => {
  const {
    pan
  } = useContext(TranslateContext);

  return pan;
};

export {
  usePan
};
