import {
  NodeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnNodeSelect = () => {
  const {
    onNodeSelect
  } = useContext(NodeContext);

  return onNodeSelect;
};

export {
  useOnNodeSelect
};
