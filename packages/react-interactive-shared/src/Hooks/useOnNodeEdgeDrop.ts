import {
  NodeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnNodeEdgeDrop = () => {
  const {
    onNodeEdgeDrop
  } = useContext(NodeContext);

  return onNodeEdgeDrop;
};

export {
  useOnNodeEdgeDrop
};
