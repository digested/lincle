import {
  ModeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useMode = () => {
  const {
    mode
  } = useContext(ModeContext);

  return mode;
};

export {
  useMode
};
