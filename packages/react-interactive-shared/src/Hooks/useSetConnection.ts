import {
  ConnectContext
} from '../Contexts';
import {
  type Connection
} from '../types';
import {
  useCallback,
  useContext
} from 'react';

const useSetConnection = (
  sourceId: string
) => {
  const {
    setConnection
  } = useContext(ConnectContext);

  return useCallback(
    (
      connection?: Connection
    ) => {
      if (setConnection) {
        setConnection(
          sourceId,
          connection
        );
      }
    },
    [
      setConnection,
      sourceId
    ]
  );
};

export {
  useSetConnection
};
