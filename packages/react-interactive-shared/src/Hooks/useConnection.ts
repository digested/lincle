import {
  ConnectContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useConnection = (
  sourceId: string
) => {
  const {
    connections = {}
  } = useContext(ConnectContext);

  return connections[sourceId];
};

export {
  useConnection
};
