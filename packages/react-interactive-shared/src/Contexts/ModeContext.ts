import defaultSettings from '../defaultSettings';
import {
  type Mode,
  type ModeType
} from '../types';
import {
  createContext
} from 'react';

const {
  mode,
  move,
  pull
} = defaultSettings.mode;

const ModeContext = createContext<ModeContextProps>({
  mode,
  move,
  pull
});

ModeContext.displayName = 'LincleInteractiveModeContext';

export {
  ModeContext
};

export type ModeContextProps = Mode & {
  givenMode?: ModeType;
  onMode?: (mode: ModeType) => void;
};
