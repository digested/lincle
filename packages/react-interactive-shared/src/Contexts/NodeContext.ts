import {
  createContext
} from 'react';

const NodeContext = createContext<NodeContextProps>({});

NodeContext.displayName = 'LincleInteractiveNodeContext';

export {
  NodeContext
};

export type NodeContextProps = {
  onNodeDrag?: (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    event: any,
    nodeId: number | string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any
  ) => void;
  onNodeEdgeDrop?: (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    event: any,
    sourceId: number | string,
    targetId?: number | string
  ) => void;
  onNodeSelect?: (
    nodeId: number | string
  ) => void;
  onNodeStart?: (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    event: any,
    nodeId: number | string
  ) => void;
  onNodeStop?: (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    event: any,
    nodeId: number | string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any
  ) => void;
};
