import defaultSettings from '../defaultSettings';
import {
  type GridType
} from '../types';
import {
  createContext
} from 'react';

const snap = defaultSettings.snap;

const SnapContext = createContext<SnapContextProps>(snap);

SnapContext.displayName = 'LincleInteractiveSnapContext';

export {
  SnapContext
};

export type SnapContextProps = false | GridType;
