import defaultSettings from '../defaultSettings';
import {
  createContext
} from 'react';

const scale = defaultSettings.scale;

const ScaleContext = createContext<ScaleContextProps>({
  scale
});

ScaleContext.displayName = 'LincleInteractiveScaleContext';

export {
  ScaleContext
};

export type ScaleContextProps = {
  onScale?: (scale: number) => void;
  scale: number;
};
