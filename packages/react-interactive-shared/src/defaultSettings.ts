import {
  type ModeType
} from './types';
import {
  defaultSettings
} from '@lincle/react-shared';

export default {
  ...defaultSettings,
  mode: {
    mode: 'move' as ModeType,
    move: true,
    pull: true
  },
  scale: 1,
  snap: false as const,
  transform: {
    maxScale: 2,
    minScale: 0.5,
    pan: true,
    zoom: true
  },
  translate: {
    pan: true,
    translate: {
      x: 0,
      y: 0
    }
  }
};
