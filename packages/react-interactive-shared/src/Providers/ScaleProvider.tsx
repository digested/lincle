import {
  ScaleContext,
  type ScaleContextProps
} from '../Contexts';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';

const ScaleProvider: FunctionComponent<ScaleProviderProps> = ({
  children,
  onScale,
  scale: givenScale
}) => {
  const [
    scale,
    setScale
  ] = useState(givenScale);

  useEffect(
    () => {
      setScale(givenScale);
    },
    [
      givenScale
    ]
  );

  const handleScale = useCallback(
    (newScale: number) => {
      if (onScale) {
        onScale(newScale);
      }

      setScale(newScale);
    },
    [
      onScale
    ]
  );

  const Scale = useMemo(
    () => {
      return {
        onScale: handleScale,
        scale
      };
    },
    [
      handleScale,
      scale
    ]
  );

  return (
    <ScaleContext.Provider
      value={Scale}
    >
      {children}
    </ScaleContext.Provider>
  );
};

ScaleProvider.displayName = 'LincleScaleProvider';

export {
  ScaleProvider
};

export type ScaleProviderProps = PropsWithChildren<ScaleContextProps>;
