import {
  TransformContext,
  type TransformContextProps
} from '../Contexts';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';

const TransformProvider: FunctionComponent<TransformProviderProps> = ({
  children,
  maxScale,
  minScale,
  zoom
}) => {
  const transform = useMemo(
    () => {
      return {
        maxScale,
        minScale,
        zoom
      };
    },
    [
      maxScale,
      minScale,
      zoom
    ]
  );

  return (
    <TransformContext.Provider
      value={transform}
    >
      {children}
    </TransformContext.Provider>
  );
};

TransformProvider.displayName = 'LincleTransformProvider';

export {
  TransformProvider
};

export type TransformProviderProps = PropsWithChildren<TransformContextProps>;
