import {
  ModeContext,
  type ModeContextProps
} from '../Contexts';
import defaultSettings from '../defaultSettings';
import {
  type ModeType
} from '../types';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo,
  useState
} from 'react';

const ModeProvider: FunctionComponent<ModeProviderProps> = ({
  children,
  mode: givenMode,
  move,
  pull
}) => {
  const [
    mode,
    setMode
  ] = useState(defaultSettings.mode.mode);

  const handleMode = useCallback(
    (
      newMode: ModeType
    ) => {
      setMode(newMode);
    },
    []
  );

  const Mode = useMemo(
    () => {
      return {
        givenMode,
        mode,
        move,
        onMode: handleMode,
        pull
      };
    },
    [
      givenMode,
      handleMode,
      mode,
      move,
      pull
    ]
  );

  return (
    <ModeContext.Provider
      value={Mode}
    >
      {children}
    </ModeContext.Provider>
  );
};

ModeProvider.displayName = 'LincleModeProvider';

export {
  ModeProvider
};

export type ModeProviderProps = PropsWithChildren<Omit<ModeContextProps, 'givenMode'>>;
