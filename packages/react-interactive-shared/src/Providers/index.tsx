import defaultSettings from '../defaultSettings';
import {
  ConnectProvider,
  type ConnectProviderProps,
  ModeProvider,
  type ModeProviderProps,
  NodeProvider,
  type NodeProviderProps,
  ScaleProvider,
  type ScaleProviderProps,
  SnapProvider,
  type SnapProviderProps,
  TransformProvider,
  type TransformProviderProps,
  TranslateProvider,
  type TranslateProviderProps
} from './Providers';
import {
  type FunctionComponent
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  maxScale = defaultSettings.transform.maxScale,
  minScale = defaultSettings.transform.minScale,
  mode,
  move = defaultSettings.mode.move,
  onNodeDrag,
  onNodeEdgeDrop,
  onNodeSelect,
  onNodeStart,
  onNodeStop,
  onScale,
  onTranslate,
  pan = defaultSettings.translate.pan,
  pull = defaultSettings.mode.pull,
  scale = defaultSettings.scale,
  snap,
  translate = defaultSettings.translate.translate,
  zoom = defaultSettings.transform.zoom
}) => {
  return (
    <SnapProvider
      snap={snap}
    >
      <ConnectProvider>
        <TransformProvider
          maxScale={maxScale}
          minScale={minScale}
          onScale={onScale}
          zoom={zoom}
        >
          <NodeProvider
            onNodeDrag={onNodeDrag}
            onNodeEdgeDrop={onNodeEdgeDrop}
            onNodeSelect={onNodeSelect}
            onNodeStart={onNodeStart}
            onNodeStop={onNodeStop}
          >
            <ModeProvider
              mode={mode}
              move={move}
              pull={pull}
            >
              <ScaleProvider
                onScale={onScale}
                scale={scale}
              >
                <TranslateProvider
                  onTranslate={onTranslate}
                  pan={pan}
                  translate={translate}
                >
                  {children}
                </TranslateProvider>
              </ScaleProvider>
            </ModeProvider>
          </NodeProvider>
        </TransformProvider>
      </ConnectProvider>
    </SnapProvider>
  );
};

Providers.displayName = 'LincleInteractiveProviders';

export {
  Providers
};

export type ProvidersProps = Partial<
ConnectProviderProps &
ModeProviderProps &
NodeProviderProps &
ScaleProviderProps &
SnapProviderProps &
TransformProviderProps &
TranslateProviderProps
>;
