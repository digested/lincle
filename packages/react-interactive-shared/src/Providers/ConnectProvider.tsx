import {
  ConnectContext
} from '../Contexts';
import {
  type Connection,
  type Connections
} from '../types';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo,
  useState
} from 'react';

const ConnectProvider: FunctionComponent<ConnectProviderProps> = ({
  children
}) => {
  const [
    connections,
    setConnections
  ] = useState<Connections>({});

  const setConnection = useCallback(
    (
      sourceId: string,
      connection?: Connection
    ) => {
      setConnections(
        (
          currentConnections
        ) => {
          if (connection) {
            return {
              ...currentConnections,
              [sourceId]: connection
            };
          } else {
            const {
              // eslint-disable-next-line @typescript-eslint/no-unused-vars
              [sourceId]: omit,
              ...rest
            } = currentConnections;

            return rest;
          }
        }
      );
    },
    []
  );

  const value = useMemo(
    () => {
      return {
        connections,
        setConnection,
        setConnections
      };
    },
    [
      connections,
      setConnection
    ]
  );

  return (
    <ConnectContext.Provider
      value={value}
    >
      {children}
    </ConnectContext.Provider>
  );
};

ConnectProvider.displayName = 'LincleConnectProvider';

export {
  ConnectProvider
};

export type ConnectProviderProps = PropsWithChildren<object>;
