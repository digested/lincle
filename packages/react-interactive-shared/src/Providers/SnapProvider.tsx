import {
  SnapContext,
  type SnapContextProps
} from '../Contexts';
import {
  useGrid
} from '@lincle/react-shared';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';

const SnapProvider: FunctionComponent<SnapProviderProps> = ({
  children,
  snap
}) => {
  const grid = useGrid() ?? false;

  const derivedSnap = useMemo(
    () => {
      switch (snap) {
        case false: {
          return false;
        }

        case undefined: {
          return grid;
        }

        default: {
          return snap;
        }
      }
    },
    [
      grid,
      snap
    ]
  );

  return (
    <SnapContext.Provider
      value={derivedSnap}
    >
      {children}
    </SnapContext.Provider>
  );
};

SnapProvider.displayName = 'LincleSnapProvider';

export {
  SnapProvider
};

export type SnapProviderProps = PropsWithChildren<{
  readonly snap?: SnapContextProps;
}>;
