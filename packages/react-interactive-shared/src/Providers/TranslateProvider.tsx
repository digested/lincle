import {
  TranslateContext,
  type TranslateContextProps
} from '../Contexts';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';

const TranslateProvider: FunctionComponent<TranslateProviderProps> = ({
  children,
  onTranslate,
  pan,
  translate: {
    x,
    y
  }
}) => {
  const [
    translate,
    setTranslate
  ] = useState({
    x,
    y
  });

  const handleTranslate = useCallback(
    (
      newTranslate: {
        x: number;
        y: number;
      }
    ) => {
      if (onTranslate) {
        onTranslate(newTranslate);
      }

      setTranslate(newTranslate);
    },
    [
      onTranslate
    ]
  );

  const Translate = useMemo(
    () => {
      return {
        onTranslate: handleTranslate,
        pan,
        translate
      };
    },
    [
      handleTranslate,
      pan,
      translate
    ]
  );

  useEffect(
    () => {
      setTranslate({
        x,
        y
      });
    },
    [
      x,
      y
    ]
  );

  return (
    <TranslateContext.Provider
      value={Translate}
    >
      {children}
    </TranslateContext.Provider>
  );
};

TranslateProvider.displayName = 'LincleTranslateProvider';

export {
  TranslateProvider
};

export type TranslateProviderProps = PropsWithChildren<TranslateContextProps>;
